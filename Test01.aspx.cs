﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using BMTMicro;

public partial class Test01 : System.Web.UI.Page
{
	private request theRequest { set; get; }
	private bool success { set; get; }
	private string errorMessage { set; get; }
	private int errorCode { set; get; }
    
	protected void Page_Load(object sender, EventArgs e)
    {
		try
		{
			//request r = new request();
			//r.theOrdernotification = new ordernotification();
			//r.theOrdernotification.ccomList = new ccom[]{ new ccom() { index = "1", text = "test"}};
			//string text = Helper.SerializeObject<request>(r);

			//r = Helper.DeserializeObject<request>("<request><ordernotification><ccom index=\"0\">Customer comments, ccom0 field</ccom><ccom index=\"1\">Customer comments, ccom1 field</ccom></ordernotification></request>");
			
			using (StreamReader sr = new StreamReader(Request.InputStream))
			{
				string xml = sr.ReadToEnd();
				if (!string.IsNullOrWhiteSpace(xml))
				{
					this.theRequest = Helper.DeserializeObject<request>(xml);

					ordernotification order = theRequest.theOrdernotification;
					customer cust = order.theCustomer;
					orderitem item = order.theOrderItem;

					//Do something with the data here...
					
					
					//xml = Helper.SerializeObject<request>(theRequest);
					
					this.success = true;
				}
				else
					throw new Exception("The XML from BMTMicro was empty?!");
			}
		}
		catch (Exception ex)
		{
			this.errorCode = 1;
			this.errorMessage = ex.Message;
		}
	}

	protected override void Render(HtmlTextWriter writer)
	{
		//No default rendering.
		//base.Render(writer);

		Response.Clear();
		Response.ContentType = "text/xml";

		if (success)
		{
			Response.Write("<response><ordernotification/></response>");
		}
		else
		{
			Response.Write("<response><ordernotification><errorcode>" + errorCode.ToString() + "</errorcode><errormessage>" + new XText(errorMessage).ToString() +
				"</errormessage></ordernotification></response>");
		}

		Response.Flush();
		//This is the correct way to end the response.
		HttpContext.Current.ApplicationInstance.CompleteRequest();
	}
}