﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

/*
<request>
   <ordernotification>
      <orderid>BMT Micro Order ID number, 7 digits or more</orderid>  
      <ordernumber>BMT Micro Long format order number</ordernumber>
      <customer>
         <billing>
             <company>Name of company, billing address</company>
             <lastname>Last name of customer, billing address</lastname> 
             <firstname>First name of customer, billing address</firstname>
             <address1>Address of customer, line 1, billing address</address1> 
             <address2>Address of customer, line 2, billing address</address2> 
             <city>City of customer, billing address</city>     
             <state>State of customer, billing address</state>    
             <zip>ZIP code of customer, billing address</zip>      
             <country>Country of customer, billing address</country>  
             <phone>Phone number of customer, billing address</phone>    
             <altphone>Alternate phone number of customer, billing address</altphone> 
             <fax>Fax number of customer, billing address</fax>      
             <email>Customer e-mail address, billing address</email>    
             <altemail>Customer alternate e-mail address, billing address</altemail> 
             <vatnumber>Customer Value Added Tax number in the European Union</vatnumber>
         </billing>
         <shipping>
             <company>Name of company, shipping address</company>
             <lastname>Last name of customer, shipping address</lastname> 
             <firstname>First name of customer, shipping address</firstname>
             <address1>Address of customer, line 1, shipping address</address1> 
             <address2>Address of customer, line 2, shipping address</address2> 
             <city>City of customer, shipping address</city>     
             <state>State of customer, shipping address</state>    
             <zip>ZIP code of customer, shipping address</zip>      
             <country>Country of customer, shipping address</country>  
             <phone>Phone number of customer, shipping address</phone>    
             <fax>Fax number of customer, shipping address</fax>      
             <email>Customer e-mail address, shipping address</email>    
         </shipping>
         <registername>The name on which the product should be registered</registername>
      </customer>
      <orderitem>
         <itemnumber>Ordinal number of item in order</itemnumber>      
         <productid>Product ID</productid>       
         <productname>Product name</productname>     
         <quantity>Quantity ordered</quantity>        
         <productprice>Price of product in product currency</productprice>    
         <productdiscount>Discount amount</productdiscount> 
         <vendorroyalty>Vendor royalty (in vendor currency)</vendorroyalty>   
         <affiliateroyalty>Affiliate royalty (in vendor currency)</affiliateroyalty>
         <bmtroyalty>BMT's royalty (in vendor currency)</bmtroyalty>      
         <vendorcurrency>Vendor currency</vendorcurrency>  
         <productcurrency>Product currency</productcurrency> 
         <affiliateid>Affiliate id</affiliateid>     
         <discountcode>Discount code</discountcode>    
         <iteminfo>Item info</iteminfo>        
         <downloadpassword>Download password</downloadpassword>
         <registrationkeys>
            <keydata keynumber="1" keypart="1">Registration key 1, part 1</keydata>
            <keydata keynumber="1" keypart="2">Registration key 1, part 2</keydata>
            <keydata keynumber="2" keypart="1">Registration key 2, part 1</keydata>
            <keydata keynumber="2" keypart="2">Registration key 2, part 2</keydata>
         </registrationkeys>
      </orderitem>
      <orderdate>The date the order was placed in ISO-8601 calendar date format</orderdate>
      <ordercurrency>The currency used for the order (payment currency)</ordercurrency>
      <ipaddress>IP-address of the remote system placing the order in decimal dotted format (xxx.xxx.xxx.xxx)</ipaddress>
      <referral>Referral field copied from order form, if present</referral>
      <orderparameters>Order parameters</orderparameters>
      <ponumber>Purchase order number</ponumber>
      <comments>Customer comments</comments>
      <howheard>Customer comments, howheard field</howheard>
      <ccom index="0">Customer comments, ccom0 field</ccom>
      <ccom index="1">Customer comments, ccom1 field</ccom>
      <ccom index="2">Customer comments, ccom2 field</ccom>
   </ordernotification>
</request>
*/

namespace BMTMicro
{
	[Serializable]
	public class request
	{
		[XmlElement(ElementName = "ordernotification")]
		public ordernotification theOrdernotification { set; get; }
	}

	[Serializable]
	public class ordernotification
	{
		public string orderid { set; get; }			//BMT Micro Order ID number, 7 digits or more
		public string ordernumber { set; get; }		//>BMT Micro Long format order number
		[XmlElement(ElementName = "customer")]
		public customer theCustomer { set; get; }
		[XmlElement(ElementName = "orderitem")]
		public orderitem theOrderItem { set; get; }
		public string orderdate { set; get; }		//The date the order was placed in ISO-8601 calendar date format
		public string ordercurrency { set; get; }	//The currency used for the order (payment currency)
		public string ipaddress { set; get; }		//IP-address of the remote system placing the order in decimal dotted format (xxx.xxx.xxx.xxx)
		public string referral { set; get; }		//Referral field copied from order form, if present
		public string orderparameters { set; get; }	//Order parameters
		public string ponumber { set; get; }		//Purchase order number
		public string comments { set; get; }		//Customer comments
		public string howheard { set; get; }		//Customer comments, howheard field
		[XmlElement(ElementName = "ccom")]
		public ccom[] ccomList { set; get; }		//<ccom index="0">Customer comments, ccom0 field</ccom>
	}

	[Serializable]
	public class customer
	{
		[XmlElement(ElementName = "billing")]
		public billing theBilling { set; get; }
		[XmlElement(ElementName = "shipping")]
		public shipping theShipping { set; get; }
		public string registername { set; get; }	//The name on which the product should be registered
	}

	[Serializable]
	public class billing
	{
		public string company { set; get; }		//Name of company, billing address
		public string lastname { set; get; }	//Last name of customer, billing address
		public string firstname { set; get; }	//First name of customer, billing address
		public string address1 { set; get; }	//Address of customer, line 1, billing address
		public string address2 { set; get; }	//Address of customer, line 2, billing address
		public string city { set; get; }		//City of customer, billing address
		public string state { set; get; }		//State of customer, billing address
		public string zip { set; get; }			//ZIP code of customer, billing address
		public string country { set; get; }		//Country of customer, billing address
		public string phone { set; get; }		//Phone number of customer, billing address
		public string altphone { set; get; }	//Alternate phone number of customer, billing address
		public string fax { set; get; }			//Fax number of customer, billing address
		public string email { set; get; }		//Customer e-mail address, billing address
		public string altemail { set; get; }    //Customer alternate e-mail address, billing address
		public string vatnumber { set; get; }	//Customer Value Added Tax number in the European Union
	}

	[Serializable]
	public class shipping
	{
		public string company { set; get; }		//Name of company, shipping address
		public string lastname { set; get; }	//Last name of customer, shipping address
		public string firstname { set; get; }	//First name of customer, shipping address
		public string address1 { set; get; }	//Address of customer, line 1, shipping address
		public string address2 { set; get; }	//Address of customer, line 2, shipping address
		public string city { set; get; }		//City of customer, shipping address
		public string state { set; get; }		//State of customer, shipping address
		public string zip { set; get; }			//ZIP code of customer, shipping address
		public string country { set; get; }		//Country of customer, shipping address
		public string phone { set; get; }		//Phone number of customer, shipping address
		public string fax { set; get; }			//Fax number of customer, shipping address
		public string email { set; get; }		//Customer e-mail address, shipping address
	}

	[Serializable]
	public class orderitem
	{
		public string itemnumber { set; get; }			//Ordinal number of item in order
		public string productid { set; get; }			//Product ID
		public string productname { set; get; }			//Product name
		public string quantity { set; get; }			//Quantity ordered
		public string productprice { set; get; }		//Price of product in product currency
		public string productdiscount { set; get; }		//Discount amount
		public string vendorroyalty { set; get; }		//Vendor royalty (in vendor currency)
		public string affiliateroyalty { set; get; }	//Affiliate royalty (in vendor currency)
		public string bmtroyalty { set; get; }			//BMT's royalty (in vendor currency)
		public string vendorcurrency { set; get; }		//Vendor currency
		public string productcurrency { set; get; }		//Product currency
		public string affiliateid { set; get; }			//Affiliate id
		public string discountcode { set; get; }		//Discount code
		public string iteminfo { set; get; }			//Item info
		public string downloadpassword { set; get; }	//Download password
		public keydata[] registrationkeys { set; get; }	//<registrationkeys><keydata keynumber="1" keypart="1">Registration key 1, part 1</keydata></registrationkeys>
	}

	public class ccom : XMLElementWithAttributes
	{
		[XmlAttribute]
		public string index { set; get; }
	}

	public class keydata : XMLElementWithAttributes
	{
		[XmlAttribute]
		public string keynumber { set; get; }
		[XmlAttribute]
		public string keypart { set; get; }
	}
	
	/// <summary>
	/// This class handles serializing and deserializing XML elements that
	/// look like this: <ccom index="0">Customer comments, ccom0 field</ccom>
	/// I could not figure out how to get the default serialization to do it.
	/// </summary>
	[Serializable]
	public class XMLElementWithAttributes : IXmlSerializable
	{
		[XmlIgnore]
		public string text { set; get; }

		public virtual void ReadXml(System.Xml.XmlReader reader)
		{
			//Handle the attributes.
			if (reader.HasAttributes)
			{
				while (reader.MoveToNextAttribute())
				{
					if (!string.IsNullOrEmpty(reader.Value))
					{
						//Use reflection to set the property.
						PropertyInfo pi = this.GetType().GetProperty(reader.Name);
						//'reader.Value' is a string so convert it to the proper type.
						if (pi != null)
							pi.SetValue(this, Convert.ChangeType(reader.Value, pi.PropertyType), null);
					}
				}
			}
			//Get the element text.
			this.text = reader.ReadElementString();
		}
	
		public void WriteXml(System.Xml.XmlWriter writer)
		{
			//Write the attributes (only members declared at the level of the supplied type's hierarchy)
			PropertyInfo[] piList = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
			if (piList.Length > 0)
			{
				for (int i = 0; i < piList.Length; i++)
				{
					object obj = piList[i].GetValue(this, null);
					if (obj != null)
						writer.WriteAttributeString(piList[i].Name, ObjectToString(obj, piList[i].PropertyType));
				}
			}
			//Write the element text.
			writer.WriteValue(text);
		}

		public System.Xml.Schema.XmlSchema GetSchema()
		{
			//For this we don't need a schema.
			return null;
		}

		static private string ObjectToString(object obj, Type type)
		{
			if (type == typeof(DateTime))
				return ((DateTime)obj).ToString("yyyy-MM-ddTHH:mm:sszzz");
			else
				return obj.ToString();
		}
	}

	/// <summary>
	/// This class has needed helper methods.
	/// </summary>
	public class Helper
	{
		/// <summary>
		/// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
		/// </summary>
		/// <param name="characters">Unicode Byte Array to be converted to String</param>
		/// <returns>String converted from Unicode Byte Array</returns>
		public static string UTF8ByteArrayToString(byte[] characters)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			string constructedString = encoding.GetString(characters);
			return (constructedString);
		}

		/// <summary>
		/// Converts the String to UTF8 Byte array and is used in De serialization
		/// </summary>
		/// <param name="pXmlString"></param>
		/// <returns></returns>
		public static Byte[] StringToUTF8ByteArray(string pXmlString)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		}

		/// <summary>
		/// Serialize an object into an XML string
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static string SerializeObject<T>(T obj)
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				XmlSerializer xs = new XmlSerializer(typeof(T));
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8))
				{
					xs.Serialize(xmlTextWriter, obj);
					return UTF8ByteArrayToString(((MemoryStream)xmlTextWriter.BaseStream).ToArray());
				}
			}
		}

		/// <summary>
		/// Reconstruct an object from an XML string
		/// </summary>
		/// <param name="xml"></param>
		/// <returns></returns>
		public static T DeserializeObject<T>(string xml)
		{
			XmlSerializer xs = new XmlSerializer(typeof(T));
			using (MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml)))
			{
				XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
				return (T)xs.Deserialize(memoryStream);
			}
		}
	}
}
