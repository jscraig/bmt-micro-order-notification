﻿<%@ Page Language="C#" ValidateRequest="false" EnableTheming="false" EnableViewState="false" %>

<%@ Import Namespace="BMTMicro" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">

    private request theRequest { set; get; }
    private bool success { set; get; }
    private string errorMessage { set; get; }
    private int errorCode { set; get; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //BMT sends an order notification XML (see example in BMTMicro.cs). Read the XML from
            // the InputStream.  Deserialize it to a BMTMicro.request object.
            using (StreamReader sr = new StreamReader(Request.InputStream))
            {
                string xml = sr.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(xml))
                {
                    this.theRequest = Helper.DeserializeObject<request>(xml);

                    ordernotification order = theRequest.theOrdernotification;
                    customer cust = order.theCustomer;
                    orderitem item = order.theOrderItem;

                    //Do something with the data here...

                    this.success = true;
                }
                else
                    throw new Exception("The XML from BMTMicro was empty?!");
            }
        }
        catch (Exception ex)
        {
            this.errorCode = 1;
            this.errorMessage = ex.Message;
        }
    }

    //Repond back from the Render method. Send back XML response.
    //
    protected override void Render(HtmlTextWriter writer)
    {
        //No default rendering.
        //base.Render(writer);

        Response.Clear();
        Response.ContentType = "text/xml";

        if (success)
        {
            Response.Write("<response><ordernotification/></response>");
        }
        else
        {
            Response.Write("<response><ordernotification><errorcode>" + errorCode.ToString() + "</errorcode><errormessage>" + new XText(errorMessage).ToString() +
                "</errormessage></ordernotification></response>");
        }

        Response.Flush();
        //This is the correct way to end the response.
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }


</script>

<html><head><title></title></head><body></body></html>