﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BMTMicro;

public partial class TestOrderNotification : System.Web.UI.Page
{
	private string responseText { set; get; }

	protected void Page_Load(object sender, EventArgs e)
    {
		string xml = "<request>" +
		"<ordernotification>" +
		"<orderid>BMT Micro Order ID number, 7 digits or more</orderid>" +
		"<ordernumber>BMT Micro Long format order number</ordernumber>" +
		"<customer>" +
		"<billing>" +
		"<company>Name of company, billing address</company>" +
		"<lastname>Last name of customer, billing address</lastname>" +
		"<firstname>First name of customer, billing address</firstname>" +
		"<address1>Address of customer, line 1, billing address</address1>" +
		"<address2>Address of customer, line 2, billing address</address2>" +
		"<city>City of customer, billing address</city>" +
		"<state>State of customer, billing address</state>"  +
		"<zip>ZIP code of customer, billing address</zip>"  +
		"<country>Country of customer, billing address</country>" +
		"<phone>Phone number of customer, billing address</phone>" +
		"<altphone>Alternate phone number of customer, billing address</altphone>" +
		"<fax>Fax number of customer, billing address</fax>" +
		"<email>Customer e-mail address, billing address</email>" +
		"<altemail>Customer alternate e-mail address, billing address</altemail>" +
		"<vatnumber>Customer Value Added Tax number in the European Union</vatnumber>" +
		"</billing>" +
		"<shipping>" +
		"<company>Name of company, shipping address</company>" +
		"<lastname>Last name of customer, shipping address</lastname>" +
		"<firstname>First name of customer, shipping address</firstname>" +
		"<address1>Address of customer, line 1, shipping address</address1>" +
		"<address2>Address of customer, line 2, shipping address</address2>" +
		"<city>City of customer, shipping address</city>" +
		"<state>State of customer, shipping address</state>" +
		"<zip>ZIP code of customer, shipping address</zip>" +
		"<country>Country of customer, shipping address</country>" +
		"<phone>Phone number of customer, shipping address</phone>" +
		"<fax>Fax number of customer, shipping address</fax>" +
		"<email>Customer e-mail address, shipping address</email>" +
		"</shipping>" +
		"<registername>The name on which the product should be registered</registername>" +
		"</customer>" +
		"<orderitem>" +
		"<itemnumber>Ordinal number of item in order</itemnumber>" +
		"<productid>Product ID</productid>" +
		"<productname>Product name</productname>" +
		"<quantity>Quantity ordered</quantity>" +
		"<productprice>Price of product in product currency</productprice>" +
		"<productdiscount>Discount amount</productdiscount>" +
		"<vendorroyalty>Vendor royalty (in vendor currency)</vendorroyalty>" +
		"<affiliateroyalty>Affiliate royalty (in vendor currency)</affiliateroyalty>" +
		"<bmtroyalty>BMT's royalty (in vendor currency)</bmtroyalty>" +
		"<vendorcurrency>Vendor currency</vendorcurrency>" +
		"<productcurrency>Product currency</productcurrency>" +
		"<affiliateid>Affiliate id</affiliateid>" +
		"<discountcode>Discount code</discountcode>" +
		"<iteminfo>Item info</iteminfo>" +
		"<downloadpassword>Download password</downloadpassword>" +
		"<registrationkeys>" +
		"<keydata keynumber=\"1\" keypart=\"1\">Registration key 1, part 1</keydata>" +
		"<keydata keynumber=\"1\" keypart=\"2\">Registration key 1, part 2</keydata>" +
		"<keydata keynumber=\"2\" keypart=\"1\">Registration key 2, part 1</keydata>" +
		"<keydata keynumber=\"2\" keypart=\"2\">Registration key 2, part 2</keydata>" +
		"</registrationkeys>" +
		"</orderitem>" +
		"<orderdate>The date the order was placed in ISO-8601 calendar date format</orderdate>" +
		"<ordercurrency>The currency used for the order (payment currency)</ordercurrency>" +
		"<ipaddress>IP-address of the remote system placing the order in decimal dotted format (xxx.xxx.xxx.xxx)</ipaddress>" +
		"<referral>Referral field copied from order form, if present</referral>" +
		"<orderparameters>Order parameters</orderparameters>" +
		"<ponumber>Purchase order number</ponumber>" +
		"<comments>Customer comments</comments>" +
		"<howheard>Customer comments, howheard field</howheard>" +
		"<ccom index=\"0\">Customer comments, ccom0 field</ccom>" +
		"<ccom index=\"1\" value1=\"1\">Customer comments, ccom1 field</ccom>" +
		"<ccom index=\"2\">Customer comments, ccom2 field</ccom>" +
		"</ordernotification>" +
		"</request>";

		//Setup the web request.
		HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:4638/BMTOrderNotification.aspx");
		myWebRequest.ContentType = "xml/text";
		myWebRequest.Method = "POST";
		
		//Turn the above XML to a byte array.
		byte[] byteArray = Helper.StringToUTF8ByteArray(xml);
		//Set the content length.
		myWebRequest.ContentLength = byteArray.Length;

		//Write the request data.
		using (Stream requestStream = myWebRequest.GetRequestStream())
		{
			requestStream.Write(byteArray, 0, byteArray.Length);
		}
		//Get the response.
		using (WebResponse myWebResponse = myWebRequest.GetResponse())
		{
			//Get the response stream.
			using (Stream responseStream = myWebResponse.GetResponseStream())
			{
				//Read the response stream.
				using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
				{
					responseText = readStream.ReadToEnd();
				}
			}
		}
    }

	protected override void Render(HtmlTextWriter writer)
	{
		//No default rendering.
		//base.Render(writer);

		Response.Clear();
		Response.ContentType = "text/xml";
		Response.Write(responseText);
		Response.Flush();
		//This is the correct way to end the response.
		HttpContext.Current.ApplicationInstance.CompleteRequest();
	}
}